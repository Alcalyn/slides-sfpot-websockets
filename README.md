# Slides websockets SfPot Août 2018

[Slides de ma présentation des websockets](https://alcalyn.gitlab.io/slides-sfpot-websockets/) au SfPot de Août 2018.

- [Version HTML](https://alcalyn.gitlab.io/slides-sfpot-websockets/)
- [Version LibreOffice](prez-sfpot-websockets.odp)
- [Version PDF](prez-sfpot-websockets.pdf)


## Licence

Ces slides sont disponibles sous la licence [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/).

![CC0 1.0 License](cc0.png)
